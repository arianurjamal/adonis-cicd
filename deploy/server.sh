# Pull code
cd /home/aria/adonis-cicd
pwd
git checkout master
git pull origin master

# Build and deploy
export PATH=/home/aria/.nvm/versions/node/v16.19.0/bin/:$PATH
npm install
# npm run build
# pm2 restart server